import styles from '../styles/Home.module.css'
import { Empty, Layout } from 'antd';
import React,{ useEffect, useState } from 'react';
import Link from 'next/Link';

const { Content } = Layout;

export default function Home() {


  //to get the data from localstorage
  const getLocalItems = () => {
    let lists = localStorage.getItem('lists');
    console.log(lists)

    if(lists) {
      return JSON.parse(lists);
    }else {
      return [];
    }
  }

  const [userInput, setUserInput] = useState('')
  const [todoList, setTodoList] = useState([]);

  //delete todo in todoList
  const handleDelete = (todo) => {
    const  updatedArr = todoList.filter
    (todoItem => todoList.indexOf(todoItem) != todoList.indexOf(todo))
    
    setTodoList(updatedArr)
  }

  //press enter and add todo in todoList
  const pressEnterSubmit = (event) => {
      if (event.key === "Enter") {
        event.preventDefault()

        setTodoList([
          userInput,
          ...todoList
        ])
        setUserInput('')
      }
    }

  const handleChange = (e) =>{
      e.preventDefault();
      setUserInput(e.target.value)
  }



  //add data to localStorage
  useEffect(() => {
    localStorage.setItem('lists', JSON.stringify(todoList))
  },[todoList]);

  return (
    <div className={styles.backgroud}>
        {/* left side */}
        <h2>AngularJS.</h2>
        <p>Example</p>
        <Link href='https://github.com/tastejs/todomvc/tree/gh-pages/examples/angularjs'>source</Link>

        <Content>
          <div className={styles.container}>
            <p className={styles.p} >todos</p><br />
            <form>
              <input className={styles.input} type="text" 
              value={userInput}
              placeholder='What needs to be done?'
              onChange={handleChange}
              onKeyDown={pressEnterSubmit}
              />
            </form>
            <div>
              <ul>
                {
                  todoList.length >=1 ? todoList.map((todo,idx) => {
                    return <li key={idx} > {todo} <button onClick={(e) => {
                      e.preventDefault()
                      handleDelete(todo)
                    }}>Delete</button></li>
                  })
                  : ""
                }
              </ul>
            </div>
          </div>
        </Content>
    </div>
  )
}
